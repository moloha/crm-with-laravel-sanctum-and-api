<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\TaskController ;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('tasks', TaskController::class);
//Route::resource('search/{name}', TaskController::class);


//Public routes
Route::post('/register',[AuthController::class,'register']);
Route::post('/login',[AuthController::class,'login']);
Route::get('/tasks', [TaskController::class, 'index']);
Route::get('/tasks/{id}', [TaskController::class, 'show']);
Route::get('/tasks/search/{name}', [TaskController::class, 'search']);
Route::get('/users',[UserController::class,'index']);

//Protected routes
Route::group(['middleware' => ['auth:sanctum']],function (){
    Route::post('/tasks', [TaskController::class,'store']);
    Route::put('/tasks/{id}', [TaskController::class,'update']);
    Route::delete('/tasks/{id}', [TaskController::class,'destroy']);
    Route::post('/logout', [AuthController::class,'logout']);

});


